# DMP-SPD

An implementation of the DMP for SPD data profiles as described in [(Abu-Dakka and Kyrki, 2020)](https://doi.org/10.1109/ICRA40945.2020.9196952).

## Software Requirements
The code is developed and tested under `Ubuntu 20.04` and `Matlab2019b`.

## References
Please acknowledge the authors if this code or part of it is useful for your research.
```
@inproceedings{abudakka2020Geometry,
	title		= {Geometry-aware dynamic movement primitives},
 	author		= {Abu-Dakka, Fares J. and Kyrki, Ville},
 	booktitle	= {IEEE International Conference on Robotics and Automation},
 	pages		= {4421--4426},
 	address		= {Paris, France},
 	year		= {2020}
}
```

## Note
This source code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY.
